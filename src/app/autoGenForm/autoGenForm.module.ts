import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from '../app.component';
import {ExampleserviceService} from './services/exampleservice.service';

@NgModule({
  declarations: [
    AppComponent  ],
  imports: [
    BrowserModule
  ],
  providers: [ExampleserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
