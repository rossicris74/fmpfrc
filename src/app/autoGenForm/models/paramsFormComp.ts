export interface paramFormComp {
    idTipoCampo:  number;
    desTipoCampo: string;
    ratioW:       number;
    height:       number;
    class:        string;    
}