import {paramFormComp} from './paramsFormComp';
export interface paramsForms {
  idForm:       number;
  eleComp:      paramFormComp[];
}