import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {ProvaComponentComponent } from './autoGenForm/components/prova-component/prova-component.component';
import { AutoGenFormComponent } from './autoGenForm/components/auto-gen-form/auto-gen-form.component';
import { DxBoxModule, 
         DxCheckBoxModule } from 'devextreme-angular';

@NgModule({
  declarations: [
    AppComponent,
    ProvaComponentComponent,
    AutoGenFormComponent
  ],
  imports: [
  BrowserModule,
  DxBoxModule,
  DxCheckBoxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
