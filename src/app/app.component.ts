import { Component,OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'FMPFRC First Project';
  developers = "Maurizio - Fabio - Cristian"
  version = "Pre-pre-pre-pre-pre-alpha"
  check1: boolean = true;
  check2: boolean = true;
  check3: boolean = true;
  check4: boolean = true;

  constructor() {

  }

  ngOnInit() {
    this.check1=false;
  }

  cambiaCheck(){
    this.check2= !this.check2;
  }

}